class JobFilterButton extends HTMLElement {
    get showcheckbox() {
        return this.getAttribute('showcheckbox');
    }
    set showcheckbox(val) {
        this.setAttribute('showcheckbox', val);
    }
    constructor() {
        super();
        var showCheckbox = this.hasAttribute('showCheckbox');
        var onlyDisplay = this.hasAttribute('onlyDisplay');
        var isChecked = this.hasAttribute('checked') && this.getAttribute('checked') === 'true';
        var text = this.getAttribute('label');
        
        var shadow = this.attachShadow({mode: 'open'});
        var wrapper = showCheckbox ? document.createElement('label') : document.createElement('div');
        
        this.hasAttribute('id') && wrapper.setAttribute('id', this.getAttribute('id'));

        var input = document.createElement('input');
        input.setAttribute('type', 'checkbox');
        input.setAttribute('disabled', true);
        var span = document.createElement('span');
        
        if (isChecked) {
            wrapper.setAttribute('class','cb_container checked');
        } else {
            wrapper.setAttribute('class','cb_container unchecked');
        }
        
        if (onlyDisplay) {
            wrapper.setAttribute('class','only_display_button');
        }
        span.setAttribute('class','checkmark');

        wrapper.innerText = text;

        input.checked = isChecked;
        
        input.addEventListener('change', e => {
            this.checkboxOnChange(input, wrapper);
        });
        
        // Create some CSS to apply to the shadow dom
        var style = document.createElement('style');

        style.textContent = `
            .cb_container {
                display: block;
                position: relative;
                cursor: pointer;
                font-size: 28px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                width: 265px;
                height: 61px;
                border-radius: 10px;
                background-color: #e8e8e8;
                margin-bottom: 15px;
                font-size: 28px;
                line-height: 61px;
                text-align: center;
                color: #777777;
            }
            
            .cb_container input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
            }
            
            .checkmark {
                position: absolute;
                top: 13px;
                left: 15px;
                height: 35px;
                width: 35px;
                background-color: #ffffff;
                border-radius: 3px;
                border: solid 1px #d8d8d8;
            }
            .checked {
                background-color: #ff3e97;
                color: #ffffff;
            }
            .unchecked {
                background-color: #e8e8e8;
                color: #777777;
            }
            
            .cb_container:hover input ~ .checkmark {
                background-color: #ffffff;
            }
            
            .cb_container input:checked ~ .checkmark {
                background-color: #ffffff;
            }
            
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }
            
            .cb_container input:checked ~ .checkmark:after {
                display: block;
            }
            
            .cb_container .checkmark:after {
                left: 10px;
                width: 10.5px;
                height: 25px;
                border-radius: 3px;
                border: solid #595959;
                border-width: 0 6px 6px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
            .filter_button {
                width: 265px;
                height: 80px;
                border-radius: 10px;
                background-color: #e8e8e8;
                margin-bottom: 15px;
                font-size: 28px;
                line-height: 80px;
                text-align: center;
                color: #777777;
                cursor: pointer;
            }
            .only_display_button {
                width: 273px;
                height: 45px;
                border-radius: 22.5px;
                border: solid 1px #1f2c52;
                background-color: #ffffff;
                font-size: 20px;
                font-weight: 700;
                line-height: 45px;
                text-align: center;
                color: #1f2c52;
                margin-bottom: 30px;
            }
        `;

        shadow.appendChild(style);
        shadow.appendChild(wrapper);
        !onlyDisplay && wrapper.appendChild(input);
        if (showCheckbox) {
            wrapper.appendChild(span);
        }
    }
    checkboxOnChange(input, wrapper) {
        if(input.checked) {
            wrapper.setAttribute('class','cb_container checked');
        } else {
            wrapper.setAttribute('class','cb_container unchecked');
        }
    }

}
window.customElements.define('job-filter-button', JobFilterButton);