class JobCard extends HTMLElement {
    constructor() {
        super();
        var categories = [];
        var jobLoc = this.getAttribute('jobLoc');
        var jobTypes = this.getAttribute('jobTypes');
        var timing = this.getAttribute('timing');
        
        // jobLoc && jobLoc.length > 0 && categories.push(this.getAttribute('jobLoc'));
        jobTypes && jobTypes.length > 0 && categories.push(this.getAttribute('jobTypes'));
        timing && timing !== 'null' && timing.length > 0 && categories.push(this.getAttribute('timing'));

        var salText = this.getAttribute('salText');
        var salAmount = this.getAttribute('salAmount');
        var salPeriod = this.getAttribute('salPeriod');

        var shadow = this.attachShadow({mode: 'open'});
        var wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'slick_item_container');
            var ahref = document.createElement('a');
            ahref.setAttribute('href', this.job_url);

                var slick_item = document.createElement('div');
                slick_item.setAttribute('class', 'slick_item');

                    var item_header = document.createElement('div');
                    item_header.setAttribute('class', 'item_header');
                        var item_header_text = document.createElement('div');
                        item_header_text.setAttribute('class', 'item_header_text');
                        item_header_text.textContent = 'FEATURED AD';

                    var item_content = document.createElement('div');
                    item_content.setAttribute('class', 'item_content');
                    
                        var item_top_container = document.createElement('div');
                        item_top_container.setAttribute('class', 'item_top_container');
                            var item_icon = document.createElement('img');
                            item_icon.setAttribute('class', 'item_icon');
                            item_icon.setAttribute('src', this.getAttribute('logo'));

                            var item_summary_container = document.createElement('div');
                            item_summary_container.setAttribute('class', 'item_summary_container');
                                var item_summary_title = document.createElement('div');
                                var item_summary_desc = document.createElement('div');
                                item_summary_title.setAttribute('class', 'item_summary_title');
                                item_summary_desc.setAttribute('class', 'item_summary_desc');
                                item_summary_title.innerHTML = this.getAttribute('jobTitle');
                                item_summary_desc.innerHTML = this.getAttribute('jobDesc');

                                var item_summary_location = document.createElement('div');
                                item_summary_location.setAttribute('class', 'item_summary_location');
                                item_summary_location.textContent = this.getAttribute('companyName');

                        var item_bottom_container = document.createElement('div');
                        item_bottom_container.setAttribute('class', 'item_bottom_container');
                            var item_category_container = document.createElement('div');
                            item_category_container.setAttribute('class', 'item_category_container');
                                //  jobTypes timing
                                
                                if(jobTypes && jobTypes.length > 0) {
                                    var item_category_jt = document.createElement('div');
                                    var icon_jt = document.createElement('img');
                                    var item_title = document.createElement('div');

                                    item_category_jt.setAttribute('class', 'item_category');
                                    icon_jt.setAttribute('src', './images/job_type.png');
                                    icon_jt.setAttribute('class', 'category_icon');
                                    
                                    item_title.textContent = this.getAttribute('jobTypes');
                                    item_category_jt.appendChild(icon_jt);
                                    item_category_jt.appendChild(item_title);
                                    item_category_container.appendChild(item_category_jt);
                                }
                                if(timing && timing !== 'null' && timing.length > 0) {
                                    var item_category_timing = document.createElement('div');
                                    var icon_jtiming = document.createElement('img');
                                    var item_title = document.createElement('div');

                                    item_category_timing.setAttribute('class', 'item_category');
                                    icon_jtiming.setAttribute('src', './images/job_timing.png');
                                    icon_jtiming.setAttribute('class', 'category_icon');
                                    
                                    item_title.textContent = this.getAttribute('timing');
                                    item_category_timing.appendChild(icon_jtiming);
                                    item_category_timing.appendChild(item_title);
                                    item_category_container.appendChild(item_category_timing);
                                }

                            var item_bottom_right_container = document.createElement('div');
                            item_bottom_right_container.setAttribute('class', 'item_bottom_right_container');
                                var item_summary_salary = document.createElement('div');
                                item_summary_salary.setAttribute('class', 'item_summary_salary');
                                    
                                    var from = document.createElement('div');
                                    from.setAttribute('class', 'from');
                                    if (salText && salText.length > 0) from.textContent = salText;

                                    var salary = document.createElement('div');
                                    salary.setAttribute('class', 'salary');
                                        var salary_num = document.createElement('div');
                                        salary_num.setAttribute('class', 'salary_num');
                                        if (salAmount && salAmount.length > 0) salary_num.textContent = salAmount;

                                        var salary_per = document.createElement('div');
                                        salary_per.setAttribute('class', 'salary_per');
                                        if (salPeriod && salPeriod.length > 0) salary_per.innerHTML = salPeriod;

                                var arrow_img = document.createElement('img');
                                arrow_img.setAttribute('class', 'arrow_img');
                                arrow_img.setAttribute('src', './images/arrow_right.svg');

                        var item_icon_container = document.createElement('div');
                        item_icon_container.setAttribute('class', 'item_icon_container');
        
        var style = document.createElement('style');
        style.textContent = this.getStyle();
        shadow.appendChild(style);
        shadow.appendChild(wrapper);
        wrapper.appendChild(ahref);
        ahref.appendChild(slick_item);
        
        this.getAttribute('isfeatured') && slick_item.appendChild(item_header);
        slick_item.appendChild(item_content);
        item_header.appendChild(item_header_text);
        item_content.appendChild(item_top_container);
        item_content.appendChild(item_bottom_container);
        item_bottom_container.appendChild(item_category_container);
        item_bottom_container.appendChild(item_bottom_right_container);

        item_content.appendChild(item_icon_container);
        item_top_container.appendChild(item_icon);
        item_top_container.appendChild(item_summary_container);
        item_summary_container.appendChild(item_summary_title);
        item_summary_container.appendChild(item_summary_desc);
        item_summary_container.appendChild(item_summary_location);
        
        item_bottom_right_container.appendChild(item_summary_salary);
        item_bottom_right_container.appendChild(arrow_img);
        item_summary_salary.appendChild(from);
        item_summary_salary.appendChild(salary);
        salary.appendChild(salary_num);
        salary.appendChild(salary_per);
    }

    get job_url() {
        if(this.hasAttribute('job_url')) {
            return this.getAttribute('job_url');
        }
        return `detail.html?jbid=${this.getAttribute('jobId')}&page=${this.getAttribute('cardPageNum')}&${this.getAttribute('param')}`;
    }
    getStyle() {
        return `
        .slick_item_container {
            width: 1080px;
            height: 100%;
            margin-bottom: 50px;
        }
        
        .slick_item {
            width: 717px;
            height: 257px;
            border-radius: 10px;
            box-shadow: 0 10px 15px 0 rgba(13, 19, 36, 0.35);
            border: solid 1px #d4d4d4;
            background-color: #ffffff;
        }
        
        .item_header {
            height: 38px;
            border-top-right-radius: 10px;
            border-top-left-radius: 10px;
            background-color: #eadd3c;
        }
        
        .item_header_text {
            font-size: 20px;
            font-weight: 700;
            font-style: normal;
            font-stretch: normal;
            line-height: 38px;
            margin-left: 20px;
            text-align: left;
            letter-spacing: normal;
            color: #1f2c52;
        }
        
        .item_content {
            padding: 10px 20px;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            height: -webkit-fill-available;
        }
        
        .item_top_container {
            width: 100%;
            display: flex;
            flex-direction: row;
            margin-bottom: 10px;
        }
        
        .item_bottom_container {
            width: 100%;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 30px;
        }
        
        .item_icon {
            min-width: 180px;
            max-width: 180px;
            height: 119px;
            object-fit: contain;
        }
        
        .item_category_container {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .category_icon {
            width: 23px;
            height: 18px;
            margin-right: 10px;
        }
        .item_category {
            height: 55px;
            min-width: 150px;
            max-width: 200px;
            border-radius: 25px;
            border: solid 1px #1f2c52;
            background-color: #ffffff;
            font-size: 1.2em;
            font-weight: 700;
            text-align: center;
            color: #1f2c52;
            margin-right: 10px;
            padding: 0 10px;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
        }
        
        .item_summary_container {
            margin-left: 20px;
            max-height: 119px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
        }
        
        .item_summary_title {
            font-size: 28px;
            font-weight: 700;
            color: #222222;
            height: 40px;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 1;
            width: 100%;
            text-align: left;
        }
        .item_summary_desc {
            font-size: 20px;
            color: #222222;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 2;
            width: 100%;
            text-align: left;
            max-height: 60px;
        }
        
        .item_summary_location {
            font-weight: 700;
            height: 21px;
            font-size: 18px;
            color: #222222;
            width: 100%;
            text-align: left;
        }
        
        .item_bottom_right_container {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        
        .item_summary_salary {
            font-size: 24px;
            font-weight: 700;
            line-height: 30px;
            color: #222222;
            text-align: right;
            margin-right: 15px;
        }
        
        .salary {
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
            text-align: right;
            line-height: 40px;
        }
        
        .salary_num {
            font-size: 28px;
            color: #ff3e97;
        }
        
        .salary_per {
            font-size: 28px;
            color: #222222;
        }
        .arrow_img {
            width: 37px;
            height: 36px;
            color: #1f2c52;
        }
        a {
            text-decoration: none;
        }
        `;
    }
}
window.customElements.define('job-card', JobCard);
