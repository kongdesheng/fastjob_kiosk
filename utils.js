function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return '';
}
function getQueryStringFromUrl(name, baseUrl) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = baseUrl.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return '';
}
function changeURLArg(arg,arg_val, baseUrl){ 
    var pattern=arg+'=([^&]*)'; 
    var replaceText=arg+'='+arg_val; 
    if(baseUrl.match(pattern)){ 
        var tmp='/('+ arg+'=)([^&]*)/gi'; 
        tmp=baseUrl.replace(eval(tmp),replaceText); 
        return tmp; 
    }else{ 
        if(baseUrl.match('[\?]')){ 
            return baseUrl+'&'+replaceText; 
        }else{ 
            return baseUrl+'?'+replaceText; 
        } 
    } 
    return url+'\n'+arg+'\n'+arg_val; 
}
function removeArgFromUrl(key, baseUrl) {
    var rtn = baseUrl.split("?")[0],
        param,
        params_arr = [],
        queryString = (baseUrl.indexOf("?") !== -1) ? baseUrl.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
function countDownToHome(sec) {
    var maxTime = sec;
    var time = maxTime;
    $('body').on('keydown mousemove touchstart click mousewheel DOMMouseScroll', function(e){
        time = maxTime; 
    });
    var interval = setInterval(() => {
        time--;
        if (time <= 0) {
            clearInterval(interval);
            location.href = "./index.html";
        }
    }, 1000);
}